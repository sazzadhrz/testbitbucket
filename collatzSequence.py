# Automate the boring stuff with python
# problem - the collatz sequence, page - 77


def collatz(number):
    if number%2 == 0:
        returnValue = number//2
        print(returnValue)
        return returnValue
    else:
        returnValue = 3*number+1
        print(returnValue)
        return returnValue


def run(input):
    if input == 1:
        return
    else:
        new = collatz(input)
        run(new)


inputVal = int(input('Enter an integer'))

run(inputVal)

